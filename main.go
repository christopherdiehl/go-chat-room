package main

import (
	"log"
	"net"
	"os"
	"time"
)

type Server struct {
	Connection chan net.Conn
}

func createServer() *Server {
	return &Server{
		Connection: make(chan net.Conn),
	}
}

func main() {
	if len(os.Args) != 2 {
		log.Fatal("Please enter a port number")
	}

	listener, err := net.Listen("tcp", "127.0.0.1:"+os.Args[1])

	if err != nil {
		log.Fatal("Error listening on port " + os.Args[1])
	}

	server := createServer()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Error accepting connection at " + time.Now().UTC().String())
		}
		server.Connection <- conn
	}

}
